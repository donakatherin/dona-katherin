from django.db import models
from django.contrib.auth.models import AbstractUser,BaseUserManager
from django.contrib.auth.hashers import make_password

class UserManager(BaseUserManager):
    def _create_user(self, email, password, **other_fields):
        """
        Create and save a user with the given email and password. And any other fields, if specified.
        """
        if not email:
            raise ValueError('An Email address must be set')
        email = self.normalize_email(email)
        
        user = self.model(email=email, **other_fields)
        user.password = make_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **other_fields):
        other_fields.setdefault('is_staff', False)
        other_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **other_fields)

    def create_superuser(self, email, password=None, **other_fields):
        other_fields.setdefault('is_staff', True)
        other_fields.setdefault('is_superuser', True)

        if other_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if other_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **other_fields)
    

class User(AbstractUser):
    username=models.CharField(max_length=100,null=True,blank=True)
    first_name=models.CharField(max_length=100,null=True,blank=True)
    last_name=models.CharField(max_length=100,null=True,blank=True)
    email = models.EmailField(max_length=255, unique=True)
    phone = models.IntegerField(null=True)
    password =models.CharField(max_length=50,null=True,blank=True)


    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['phone']
    objects=UserManager()

    def get_username(self):
        return self.email 

class category(models.Model):
    Category_Name=models.CharField(max_length=50,null=True,blank=True)
   
    def __str__(self):
        return self.Category_Name 


class product(models.Model):
    Product_Name=models.CharField(max_length=100,null=True,blank=True)
    Product_Image1=models.ImageField(upload_to='images/',max_length=100,null=True,blank=True)
    Product_Description=models.CharField(max_length=100,null=True,blank=True)
    price=models.FloatField(max_length=50,null=True,blank=True)
    stock=models.IntegerField(null=True,blank=True)
    Category=models.ForeignKey('category',on_delete=models.CASCADE,null=True)
    Stock_Status=models.CharField(max_length=50,null=True,blank=True)
   
    def __str__(self):
        return self.Product_Name 


class order(models.Model):
    Order_Date=models.DateTimeField(max_length=12,null=True)
    Customer_Id=models.ForeignKey('User',on_delete=models.CASCADE,null=True)
    Delivery_Date=models.DateTimeField(max_length=12,null=True)
    Product_Id=models.ForeignKey('product',on_delete=models.CASCADE,null=True)
    total_qty=models.IntegerField(null=True)
    subtotal=models.FloatField(max_length=50,null=True,blank=True)
    Payment_method=models.CharField(max_length=50,null=True,blank=True)
    Payment_Status=models.BooleanField()
