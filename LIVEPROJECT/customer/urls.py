from django.urls import path
from customer.views import *

urlpatterns = [
    path('',logadmin,name="log"),
    path('register',signadmin,name="sign"),
    path('home',index,name="index"),
    path('logout' ,adminout,name="logout"),
   
]