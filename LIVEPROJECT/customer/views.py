from django.http.response import HttpResponse
from django.shortcuts import render,redirect
from .models import *
from django.core.files.storage import FileSystemStorage
from django.contrib.auth import authenticate,login,logout

def logadmin(request):
    if request.method=="POST":
        e=request.POST.get("uemail")
        p=request.POST.get("upass")  
        user=authenticate(request,email=e,password=p)   
        if user:
           login(request,user)
           return render(request,"index.html")
        else:
           return HttpResponse("Invalid Email or Password")
    return render(request,"login.html")

def signadmin(request):
    if request.method=="POST":
        n=request.POST.get("usname")
        e=request.POST.get("uemail")
        p=request.POST.get("upass")
        User.objects.create_user(username=n,email=e,password=p,)
        return render(request,"login.html")
    return render(request,"register.html")

def index(request): 
    return render(request,"index.html")

def adminout(request):
    logout(request)
    return redirect('log')
